//Назаров А.О М3104 Вариант 15
#include <stdio.h>
#include <string.h>

int main() {

//1. Подсчитать количество символов цифр, а также символов строчных и прописных букв английского алфавита в строке,
// введённой с консоли. Результат вывести на консоль.

    char str[1000];
    scanf("%s",str);
    int digits = 0, s_letters = 0, b_letters = 0;
    for (int i = 0; i<strlen(str); i++){
        if (str[i]>'0' && str[i]<'9')
            digits++;
        if (str[i]>'a' && str[i]<'z')
            s_letters++;
        if (str[i]>'A' && str[i]<'Z')
            b_letters++;
    }
    printf("Digits: %d\n",digits);
    printf("Small letters: %d\n",s_letters);
    printf("Big letters: %d\n",b_letters);
    printf("\n");

//5. Вывести на консоль состояние банковского счёта по истечении каждого полного месяца, при нахождении средств
// на счёте в течение введённого с консоли количества месяцев. Годовую процентную ставку и начальную сумму на счёте
// ввести с консоли. Моделируемый счёт является счётом с ежемесячной выплатой процентов и последующей капитализацией,
// а также не предусматривает частичное снятие средств.

    float i_amount;
    float percent;
    int time;
    printf("Input initial amount: ");
    scanf("%f", &i_amount);
    printf("Input annual percents: ");
    scanf("%f", &percent);
    printf("Input deposit time: ");
    scanf("%d", &time);
    int month;
    for (month = 0; month < time; month++) {
        i_amount =  (i_amount * (1 + percent / 100.0));
        printf("%d: %.2f\n",month+1, i_amount);
    }
    return 0;
}
