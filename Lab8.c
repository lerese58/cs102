//Назаров А.О М3104 Вариант 15
#include <stdio.h>
#include <string.h>

int main() {

//2. Осуществить конкатенация (сложение) первой строки и n начальных символов второй строки.

   char str1[20];
   char str2[20];
   int n;
   printf("First string: ");
   scanf("%s", str1);
   printf("Second string: ");
   scanf("%s", str2);
   printf("Count of symbols: ");
   scanf("%d", &n);
   strncat(str1, str2, n);
   printf("Result of concat: %s\n",str1);

// 5. Осуществить копирование одной строки в другую строку.
   char str3[20];
   char str4[20];
   printf("3rd strind: ");
   scanf("%s",str3);
   strcpy(str4,str3);
   printf("Result of copy: %s\n",str4);

// 8. Осуществить поиск в строке первого вхождения указанного символа. 

   char s;
   char str[20];
   printf("Input string to find: ");
   scanf("%s",str);
   printf("Symbol: ");
   scanf(" %c",&s);
   printf("1st occurence of symbol %c is %u\n",s,(strchr(str,s)-str+1));

//11. Определить длину отрезка одной строки, содержащего символы из множества символов, входящих во вторую строку.

   char str5[20];
   char str6[20];
   printf("Input string:");
   scanf("%s",str5);
   printf("Input one more: ");
   scanf("%s",str6);
   int length;
   length = strspn(str6, str5);
   printf("Character where strings intersect is at position %u\n", length);

//13. Выделить из одной строки лексемы (кусочки), 
//разделенные любым из множества символов (разделителей), входящих во вторую строку. 

   char str7[20];
   char str8[20];
   printf("Input string to separate: ");
   scanf("%s",str7);
   printf("Input string-separator: ");
   scanf("%s",str8);
   char *str9 = strtok(str7, str8);
   while (str9!= NULL){
      printf("\n%s",str9);
      str9 = strtok(NULL, str8);
   }
 return 0;
}