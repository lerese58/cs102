#include <stdio.h>
#include <stdlib.h>
#include "Lab11geometry.h"

int main() {
    struct Romb abcd;
    printf("Input coordinates of point A: \n");
    printf("x: ");
    scanf("%i",&abcd.h1.x);
    printf("y: ");
    scanf("%i",&abcd.h1.y);
    printf("Input coordinates of point B: \n");
    printf("x: ");
    scanf("%i",&abcd.h2.x);
    printf("y: ");
    scanf("%i",&abcd.h2.y);
    printf("Input coordinates of point C: \n");
    printf("x: ");
    scanf("%i",&abcd.h3.x);
    printf("y: ");
    scanf("%i",&abcd.h3.y);
    printf("Input coordinates of point D: \n");
    printf("x: ");
    scanf("%i",&abcd.h4.x);
    printf("y: ");
    scanf("%i",&abcd.h4.y);
    show(abcd);
    printf("Square of abcd = %.3f\n",square(abcd));
    printf("Perimetr of abcd  = %.3f\n",perimetr(abcd));
    system("pause");
    return 0;
}

