//Лабораторная работа №2, вариант 1, Назаров А.О М3104
#include <stdio.h>
#include <math.h>

const double PI = 3.14159265358979;

int main()
{
	double argument;
	scanf("%lf", &argument);
	double z1 = 2 * sin(3 * PI - 2 * argument)*sin(3 * PI - 2 * argument)*cos(5 * PI + 2 * argument)*cos(5 * PI + 2 * argument);
	double z2 = 0.25 - 0.25 * (sin(5 * PI / 2 - 8 * argument));
	printf("%lf\n", z1);
	printf("%lf\n", z2);
	return 0;
}