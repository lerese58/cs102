

#ifndef LAB11_GEOMETRY_H
#define LAB11_GEOMETRY_H
struct point{int x,y;};

struct Romb{struct point h1,h2,h3,h4;};

float perimetr(struct Romb abcd);

float square(struct Romb abcd);

void show(struct Romb ABCD);
#endif //LAB11_GEOMETRY_H
