//Назаров А.О М3104 Вариант 15
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

const int MAX_SIZE = 256;
int NOD(int a,int b);
int NOK(int a,int b);
void deleter(char *s,int pos);

int main() {

//Задание 1.

    int fst,snd;
    printf("Input 1st number: ");
    scanf("%i", &fst);
    printf("Input 2st number: ");
    scanf("%i", &snd);
    printf("NOD = %i\n",NOD(fst,snd));
    printf("NOK = %i\n",NOK(fst,snd));

    fflush(stdin);

//Задание 5.

    char str[MAX_SIZE];
    int i;
    fgets(str,MAX_SIZE,stdin);
    for (i=0;i<strlen(str);i++) {
        if (((str[i]==' ')&&(str[i+1]==' '))||((str[i]=='(')&&(str[i+1]==' '))||((str[i]=='"')&&(str[i+1]==' '))||((str[i]=='.')&&(str[i+1]==' ')&&(i==MAX_SIZE-1)))
            deleter(str,i+1);
    }
    printf("%s", str);
    system("pause");
    return 0;
}

int NOD(int a,int b){
    int delitel;
    for (int i = 1; i <(a<b?a:b)+1 ; i++)
        if ((a%i) == 0 && (b%i) == 0)
            delitel=i;
    return delitel;
};

int NOK(int a,int b){
    int kratnoe;
    for (int i = a*b; i > (a>b?a:b)-1; i--)
        if (i%a == 0 && i%b == 0)
            kratnoe=i;
    return kratnoe;
};
void deleter(char s[],int pos){
    int i;
    if ((s[pos]==' ')&&(s[pos+1])==' ')
        deleter(s,pos+1);
    for (i = pos; i < strlen(s)-1; i++)
        s[i]=s[i+1];
    s[i]=0;
}

