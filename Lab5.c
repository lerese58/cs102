// Лабораторная№5, вариант 1, Назаров А.О М3104
#include <stdio.h>
#include <stdlib.h>

int main() {
	int arr[] = { 10,20,30,40,50,60,70 };
	int i, j, k;
	int brr[2][2] = { { 1,2 },{ 3,4 } };
	int crr[2][2] = { { 1,0 },{ 0,1 } };
	int drr[2][2] = { { 0,0 },{ 0,0 } };
	printf("1st array\n");
	for (i = 0; i<7; i++)
	{
		printf("%d%c", arr[i], ' ');
	}
	printf("\n");
	printf("MATRIX\n");
		for (i = 0; i < 2; i++) {
		for (j = 0; j < 2; j++) {
			for (k = 0; k < 2; k++) {
				drr[i][j] += brr[i][k] * crr[k][j];
			}
			printf("%d", drr[i][j]);
			printf("%c", ' ');
		}
		printf("\n");
	}
	return 0;
}