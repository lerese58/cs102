#include <stdlib.h>


int main()
{
	printf("Pointer\n");

	int arr[4];
	int *ptr_arr = arr;
	*ptr_arr = -1;
	*(ptr_arr + 1) = -12;
	*(ptr_arr + 2) = -123;
	*(ptr_arr + 3) = -1234;
	int i;
	for (i = 0; i < 4; i++)
		printf("%d ", *(ptr_arr + i));
	
	printf("\n");
	printf("Dynamic memory\n");

	int *ptr_int;
	ptr_int = malloc(sizeof(int)*4);
	*ptr_int = -1;
	*(ptr_int + sizeof(int)) = -12;
	*(ptr_int + 2*sizeof(int)) = -123;
	*(ptr_int + 3*sizeof(int)) = -1234;
	for (i = 0; i < 4; i++)
			printf("%d ", *(ptr_int + i*sizeof(int)));
	free(*ptr_int);
	system("pause");
	return 0;
	
}
