
#include <stdio.h>
#include <math.h>
#include "Lab11geometry.h"

void show(struct Romb ABCD) {
    printf("A(%i,%i)\n",ABCD.h1.x,ABCD.h1.y);
    printf("B(%i,%i)\n",ABCD.h2.x,ABCD.h2.y);
    printf("C(%i,%i)\n",ABCD.h3.x,ABCD.h3.y);
    printf("D(%i,%i)\n",ABCD.h4.x,ABCD.h4.y);
}
float perimetr(struct Romb abcd) {
    return sqrt(pow(abcd.h1.x-abcd.h2.x,2)+pow(abcd.h1.y-abcd.h2.y,2)) //h1h2
         + sqrt(pow(abcd.h3.x-abcd.h4.x,2)+pow(abcd.h3.y-abcd.h4.y,2)) //h3h4
         + sqrt(pow(abcd.h3.x-abcd.h2.x,2)+pow(abcd.h3.y-abcd.h2.y,2)) //h2h3
         + sqrt(pow(abcd.h1.x-abcd.h4.x,2)+pow(abcd.h1.y-abcd.h4.y,2));//h1h4
}
float square(struct Romb abcd) {
    return sqrt(pow(abcd.h1.x-abcd.h3.x,2)+pow(abcd.h1.y-abcd.h3.y,2)) //h1h3
         * sqrt(pow(abcd.h2.x-abcd.h4.x,2)+pow(abcd.h2.y-abcd.h4.y,2)) //h2h4
         * 0.5;
}
