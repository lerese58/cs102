#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main(int argc, char** argv[]) {
    time_t t = time(NULL);
    struct tm* aTm = localtime(&t);
    FILE *fp;
    if((fp=fopen("in.txt", "w"))==NULL) {
        printf ("Cannot open file.\n");
        exit(1);
    }
    for (int i = 0; i < 10 ; i++) {
        fprintf(fp,"%04d/%02d/%02d %02d:%02d:%02d\n", aTm->tm_year+1900, aTm->tm_mon+1, aTm->tm_mday, aTm->tm_hour, aTm->tm_min, aTm->tm_sec);
        aTm->tm_mday++;
    }
    fclose(fp);
    return 0;
}